/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author michealtran
 */
public class NguoiDung {
    private String tennguoidung;
    private String matkhau;
    private String vaitro;

    public NguoiDung(){
        
    }

    public NguoiDung(String tennguoidung, String matkhau, String vaitro) {
        this.tennguoidung = tennguoidung;
        this.matkhau = matkhau;
        this.vaitro = vaitro;
    }

    public String getTennguoidung() {
        return tennguoidung;
    }

    public void setTennguoidung(String tennguoidung) {
        this.tennguoidung = tennguoidung;
    }

    public String getMatkhau() {
        return matkhau;
    }

    public void setMatkhau(String matkhau) {
        this.matkhau = matkhau;
    }

    public String getVaitro() {
        return vaitro;
    }

    public void setVaitro(String vaitro) {
        this.vaitro = vaitro;
    }    
}
