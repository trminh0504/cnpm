/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author michealtran
 */
public class BangDiem {
    private String mabangdiem;
    private String mahs;
    private String diemtongket;
    private String hanhkiem;
    private String hocluc;
    private String nhanxet;
    private String magv;
    
    public BangDiem(){
        
    }

    public BangDiem(String mabangdiem, String mahs, String diemtongket, String hanhkiem, String hocluc, String nhanxet, String magv) {
        this.mabangdiem = mabangdiem;
        this.mahs = mahs;
        this.diemtongket = diemtongket;
        this.hanhkiem = hanhkiem;
        this.hocluc = hocluc;
        this.nhanxet = nhanxet;
        this.magv = magv;
    }
    
    

    public String getMabangdiem() {
        return mabangdiem;
    }

    public void setMabangdiem(String mabangdiem) {
        this.mabangdiem = mabangdiem;
    }

    public String getMahs() {
        return mahs;
    }

    public void setMahs(String mahs) {
        this.mahs = mahs;
    }

    public String getDiemtongket() {
        return diemtongket;
    }

    public void setDiemtongket(String diemtongket) {
        this.diemtongket = diemtongket;
    }

    public String getHanhkiem() {
        return hanhkiem;
    }

    public void setHanhkiem(String hanhkiem) {
        this.hanhkiem = hanhkiem;
    }

    public String getHocluc() {
        return hocluc;
    }

    public void setHocluc(String hocluc) {
        this.hocluc = hocluc;
    }

    public String getNhanxet() {
        return nhanxet;
    }

    public void setNhanxet(String nhanxet) {
        this.nhanxet = nhanxet;
    }

    public String getMagv() {
        return magv;
    }

    public void setMagv(String magv) {
        this.magv = magv;
    }
    
}
