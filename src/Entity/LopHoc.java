/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author michealtran
 */
public class LopHoc {
    private String malop;
    private String tenlop;
    private String siso;
    private String namhoc;
    
    public LopHoc(){
        
    }

    public LopHoc(String malop, String tenlop, String siso, String namhoc) {
        this.malop = malop;
        this.tenlop = tenlop;
        this.siso = siso;
        this.namhoc = namhoc;
    }

    public String getMalop() {
        return malop;
    }

    public void setMalop(String malop) {
        this.malop = malop;
    }

    public String getTenlop() {
        return tenlop;
    }

    public void setTenlop(String tenlop) {
        this.tenlop = tenlop;
    }

    public String getSiso() {
        return siso;
    }

    public void setSiso(String siso) {
        this.siso = siso;
    }

    public String getNamhoc() {
        return namhoc;
    }

    public void setNamhoc(String namhoc) {
        this.namhoc = namhoc;
    }
    
}
