/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.BangDiem;
import TienIch.KetNoi;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michealtran
 */
public class DaoBangDiem {
   public static List<BangDiem> getallTT(String query) throws SQLException, ClassNotFoundException{
        List<BangDiem> lstBangDiem = new ArrayList<>();
        ResultSet rs = KetNoi.ketnoi("QLTH").createStatement().executeQuery(query);
        while (rs.next()) {
           BangDiem bd = new BangDiem();
           bd.setMabangdiem(rs.getString(1));
           bd.setMahs(rs.getString(2));
           bd.setDiemtongket(rs.getString(3));
           bd.setHanhkiem(rs.getString(4));
           bd.setHocluc(rs.getString(5));
           bd.setNhanxet(rs.getString(6));
           bd.setMagv(rs.getString(7));
       }
        return lstBangDiem;       
    }
   
    public static List<BangDiem> listBD() throws SQLException, ClassNotFoundException{
        String sql = "Select * from BangDiem";
        return getallTT(sql);
    }
}
