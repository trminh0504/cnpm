/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import Entity.BangDiem;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author michealtran
 */
public class ViewBangDiem {
    public static void loadTable(JTable tbl, List list){
        List<BangDiem> lstBangDiem = list;
        Object[] CotName = new Object[]{
            "Mã bảng điểm", "Mã học sinh", "Điểm tổng kết", "Hạnh kiểm", "Học lực", "Nhận xét", "Mã giáo viên"
        };
        Object[][] data = new Object[lstBangDiem.size()][7];
        int i=0;
        for(BangDiem bd : lstBangDiem){
            data[i][0] = bd.getMabangdiem();
            data[i][1] = bd.getMahs();
            data[i][2] = bd.getDiemtongket();
            data[i][3] = bd.getHanhkiem();
            data[i][4] = bd.getHocluc();
            data[i][5] = bd.getNhanxet();
            data[i][6] = bd.getMagv();
            i++;
        }
        TableModel tableModel = new DefaultTableModel(data, CotName);
        tbl.setModel(tableModel);
    }
    
    public static void hienthiBangDiem(JTable tbl) throws SQLException, ClassNotFoundException{
        List<BangDiem> lstBangDiem = DAO.DaoBangDiem.listBD();
        loadTable(tbl, lstBangDiem);
    }
}


